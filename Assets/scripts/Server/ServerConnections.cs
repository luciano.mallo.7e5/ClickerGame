using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System;

public class ServerConnections : MonoBehaviour
{
    public Action OnPlayerDataFilled;
    public Action OnUserUpdated;
    public Action<string> OnError;
    private const string baseUrl = "http://yolo33-env.eba-tx7zxppg.us-east-1.elasticbeanstalk.com:8000/";

    void Start()
    {
        // StartCoroutine(GetAllUsers());
        // StartCoroutine(GetOneUserCorutine("Alice", "password123"));
        // StartCoroutine(UpdateUserName("Alice","Roberta"));
        // StartCoroutine(AddNewUser("kojima", "MetalGear"));
    }

    public void GetAllPlayersData()
    {
        StartCoroutine(GetAllUserCorutines());
    }

    public void GetPlayerData(string userName, string password)
    {
        StartCoroutine(GetOneUserCorutine(userName, password));
    }

    public void UpdateUserName(string oldUserName, string newUserName, string password)
    {
        StartCoroutine(UpdateUserNameCorutine(oldUserName, newUserName, password));
    }

    public void AddNewUser(string newUser, string newPass)
    {
        StartCoroutine(AddNewUserCorutine(newUser, newPass));
    }

    public void DeleteUserScore(string userName)
    {
        StartCoroutine(DeleteUserScoreCorutine(userName));
    }

    public void UpdateScoreOfAnUser(string userName, int score)
    {
        StartCoroutine(UpdateScoreOfAnUserCorutine(userName, score));
    }

    private void UpdatePlayerData(PlayerObj player)
    {
        GameManager.Instance.playerCurrentData = player;
    }
    private IEnumerator GetOneUserCorutine(string username, string passw)
    {
        string userName = username;
        string password = passw;

        // string URL = $"http://localhost:8000/player?userName={userName}&password={password}".Normalize();
        string URL = baseUrl + $"player?userName={userName}&password={password}";
        // Obtener un user
        UnityWebRequest server = new UnityWebRequest(URL);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();

        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
            OnError?.Invoke("Error: couldn't get user");
        }
        else
        {
            string jsonString = server.downloadHandler.text;
            // Deserialize Player  
            PlayerObj player = JsonConvert.DeserializeObject<PlayerObj>(jsonString);
            UpdatePlayerData(player);
            OnPlayerDataFilled?.Invoke();
        }
    }
    private void UpdateAllPlayersData(PlayerObj[] players)
    {
        GameManager.Instance.playersList = players;
    }
    private IEnumerator DeleteUserScoreCorutine(string username)
    {
        string userName = username;
        string url = baseUrl + "delete/score?userName=" + userName;
        UnityWebRequest server = new UnityWebRequest(url);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();
        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
        }
        else
        {
            // Show results as text

            string jsonString = server.downloadHandler.text;
            // Deserialize Players
            PlayerObj player = JsonConvert.DeserializeObject<PlayerObj>(jsonString);
            // DebugPlayer(player);
        }

    }
    private IEnumerator GetAllUserCorutines()
    {
        string URL = baseUrl + "players";
        // UnityWebRequest server = new UnityWebRequest("http://localhost:8000/players");
        UnityWebRequest server = new UnityWebRequest(URL);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();
        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
        }
        else
        {
            string jsonString = server.downloadHandler.text;
            // Deserialize Players
            PlayerObj[] Allplayers = JsonConvert.DeserializeObject<PlayerObj[]>(jsonString);
            UpdateAllPlayersData(Allplayers);
        }
    }

    private IEnumerator UpdateUserNameCorutine(string oldName, string newName, string password)
    {
        string oldUserName = oldName;
        string newUserName = newName;
        string url = baseUrl + "update/player?userName=" + oldUserName + "&password=" + password + "&userName=" + newUserName;
        UnityWebRequest server = new UnityWebRequest(url);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();
        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
            OnError?.Invoke("ERROR: couldn't update user");
        }
        else
        {
            // Show results as text

            string jsonString = server.downloadHandler.text;
            // Deserialize Players
            PlayerObj player = JsonConvert.DeserializeObject<PlayerObj>(jsonString);
            // DebugPlayer(player);

            OnUserUpdated?.Invoke();
        }

    }

    private IEnumerator AddNewUserCorutine(string newUser, string newPass)
    {

        //add user: http://localhost:8000/add?userName=kojima&password=MetalGear;
        string userName = newUser;
        string password = newPass;
        string url = baseUrl + "add?userName=" + userName + "&password=" + password;
        UnityWebRequest server = new UnityWebRequest(url);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();
        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
            OnError?.Invoke("ERROR: Couldn't add user");
        }
        else
        {
            // Show results as text

            string jsonString = server.downloadHandler.text;
            // Deserialize Players
            PlayerObj player = JsonConvert.DeserializeObject<PlayerObj>(jsonString);
            // DebugPlayer(player);

            UpdatePlayerData(player);
            OnPlayerDataFilled?.Invoke();
        }

    }

    private IEnumerator UpdateScoreOfAnUserCorutine(string newUser, int score)
    {

        string userName = newUser;
        int newScore = score;
        string url = baseUrl + "update/score?userName=" + userName + "&score=" + score;
        UnityWebRequest server = new UnityWebRequest(url);
        server.downloadHandler = new DownloadHandlerBuffer();
        yield return server.SendWebRequest();
        if (server.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(server.error);
        }
        else
        {
            // Show results as text

            string jsonString = server.downloadHandler.text;
            // Deserialize Players
            PlayerObj player = JsonConvert.DeserializeObject<PlayerObj>(jsonString);
            // DebugPlayer(player);
        }

    }

    private void DebugPlayer(PlayerObj player)
    {
        Debug.Log("***************************");
        Debug.Log("userName: " + player.userName);
        Debug.Log("password: " + player.password);
        Debug.Log("ID: " + player.id);
        Debug.Log("score: " + player.score);
        Debug.Log("***************************");
    }

}
