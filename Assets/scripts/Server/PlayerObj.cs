using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerObj
{
    public int id;
    public string userName;
    public string password;
    public int score;

    /* public PlayerObj()
     {
         userName = "AxeWarrior";
         //_password = "robert22";
         score = 90;
     }*/

    public PlayerObj(string userName, string password, int? score)
    {
        this.userName = userName;
        this.password = password;
        this.score = score ?? this.score;

    }
}
