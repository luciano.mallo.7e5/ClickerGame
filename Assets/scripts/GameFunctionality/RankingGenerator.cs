using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _userEntry;
    [SerializeField] private Transform _contentTransform;
    private List<UserEntry> _userEntries = new();

    private void OnEnable()
    {
        // Here We call the server to run the corutine to get all the players.
        GameObject.FindGameObjectWithTag("Server").GetComponent<ServerConnections>().GetAllPlayersData();
    }
    private void Update()
    {
        if (GameManager.Instance.playersList != null)
            GenerateEntries(GetPlayerObjs());
    }

    private PlayerObj[] GetPlayerObjs()
    {
        // Here we get the users leaderboard
        return GameManager.Instance.playersList;
    }

    private void GenerateEntries(PlayerObj[] data)
    {
        for (int i = 0; i < data.Length; i++)
        {
            if (_userEntries.Count < data.Length)
                _userEntries.Add(Instantiate(_userEntry, _contentTransform).GetComponent<UserEntry>());

            _userEntries[i].UpdateValues(data[i].userName, data[i].score);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_contentTransform.GetComponent<RectTransform>());
    }



}
