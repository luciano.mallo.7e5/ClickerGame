using System.Collections;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText, _multiplierText, _userNameText, _errorMessage;
    [SerializeField] private Button _multiplierButton;
    [SerializeField] private Image _multiplierMask;
    [SerializeField] private Animator _mainButtonAnimator, _multiplierAnimator, _characterAnimator;
    [SerializeField] private AudioSource _mainButtonAudioSource, _multiplierAudioSource;
    [SerializeField] private GameObject _clickPanel, _rankingPanel, _inicioPanel, _optionPanel;
    [SerializeField] private TMP_InputField _inputUsernameInitPanel, _inputPasswordInitPanel, _inputOldUsernameOptionPanel, _inputNewUsernameOptionPanel, _inputPasswordOptionPanel;
    private ServerConnections _server;

    private void Awake()
    {
        _server = GameObject.FindGameObjectWithTag("Server").GetComponent<ServerConnections>();
    }

    private void Start()
    {
        UpdateScoreText();
        UpdateUserNameText();
        ResetMultiplierText();
    }

    private void OnEnable()
    {
        _server.OnPlayerDataFilled += GoToClickerPanel;
        _server.OnUserUpdated += OnGoBackClick;
        _server.OnError += ErrorMessage;
    }

    private void OnDisable()
    {
        _server.OnPlayerDataFilled -= GoToClickerPanel;
        _server.OnUserUpdated -= OnGoBackClick;
        _server.OnError -= ErrorMessage;
    }

    private void Update()
    {
        _characterAnimator.speed = Mathf.Min(GameManager.Instance.CurrentPoints / 500, float.MaxValue);
    }

    public void OnClick()
    {
        GameManager.Instance.AddPoints();
        UpdateScoreText();
        _mainButtonAnimator.SetTrigger("OnClick");
        _mainButtonAudioSource.Play();
    }

    public async void OnMultiplierClick()
    {
        _multiplierButton.interactable = false;
        _multiplierAnimator.SetBool("IsActive", true);
        _multiplierAudioSource.Play();
        _multiplierText.text = "GO!";
        _multiplierText.color = Color.white;

        await GameManager.Instance.ActivateMultiplier();
        _multiplierAnimator.SetBool("IsActive", false);
        _multiplierAudioSource.Stop();

        float cooldown = GameManager.Instance.MultiplierActiveSeconds;
        _multiplierMask.fillAmount = 1;

        while (cooldown > 0)
        {
            await Task.Yield();
            cooldown -= Time.deltaTime;
            _multiplierMask.fillAmount = cooldown / GameManager.Instance.MultiplierActiveSeconds;
            _multiplierText.text = ((int)cooldown).ToString();
        }

        _multiplierButton.interactable = true;
        _multiplierText.color = new Color32(120, 0, 0, 255);
        ResetMultiplierText();
    }

    private void ResetMultiplierText() => _multiplierText.text = "x" + GameManager.Instance.BoostMultiplier.ToString();

    private void UpdateScoreText() => _scoreText.text = "Score: " + GameManager.Instance.CurrentPoints;

    public void OnLoginClick()
    {
        string userName = _inputUsernameInitPanel.text;
        string password = _inputPasswordInitPanel.text;
        // LLamar a la api para iniciar sesion si sale bien vamos al click panel sino mostrar un cartel de error.
        _server.GetPlayerData(userName, password);
    }

    private void GoToClickerPanel()
    {
        if (GameManager.Instance.playerCurrentData != null)
        {
            _inicioPanel.SetActive(false);
            _clickPanel.SetActive(true);

            GameManager.Instance.CurrentPoints = GameManager.Instance.playerCurrentData.score;
            UpdateUserNameText();
            UpdateScoreText();
        }
        else
        {
            Debug.Log("No hay user");
        }
    }

    public void OnSignUpClick()
    {
        string userName = _inputUsernameInitPanel.text;
        string password = _inputPasswordInitPanel.text;
        // Llamar a la funcion del server para a�adir un usuario. sino mostrar un cartel de error.
        _server.AddNewUser(userName, password);
    }
    public void OnOptionsClick()
    {
        _inicioPanel.SetActive(false);
        _optionPanel.SetActive(true);
    }
    public void OnRankingClick()
    {
        _inicioPanel.SetActive(false);
        _rankingPanel.SetActive(true);
    }

    public void OnGoBackClick()
    {
        _inicioPanel.SetActive(true);
        _optionPanel.SetActive(false);
        _rankingPanel.SetActive(false);
    }
    public void OnChangeClick()
    {
        string oldUserName = _inputOldUsernameOptionPanel.text;
        string userName = _inputNewUsernameOptionPanel.text;
        string password = _inputPasswordOptionPanel.text;

        _server.UpdateUserName(oldUserName, userName, password);

        // LLamar a la api para cambiar el usuario si sale bien volvemos a inicio  sino mostrar un cartel de error.
    }
    public void OnLogOutClick()
    {
        _inicioPanel.SetActive(true);
        _clickPanel.SetActive(false);
        _server.UpdateScoreOfAnUser(GameManager.Instance.playerCurrentData.userName, (int)GameManager.Instance.CurrentPoints);
    }

    private void UpdateUserNameText()
    {
        _userNameText.text = GameManager.Instance.playerCurrentData.userName;
    }

    public void ErrorMessage(string message)
    {
        _errorMessage.text = message;
        StartCoroutine(ShowError());
    }

    private IEnumerator ShowError()
    {
        _errorMessage.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        _errorMessage.gameObject.SetActive(false);
    }
}
