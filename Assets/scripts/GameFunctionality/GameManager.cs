using System.Threading.Tasks;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public PlayerObj playerCurrentData;
    public PlayerObj[] playersList = new PlayerObj[0];
    public float CurrentPoints;
    public float PointsPerClick;
    public float BoostMultiplier;
    public int MultiplierActiveSeconds;

    private bool _isMultiplierActive = false;

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }

    public void AddPoints() => CurrentPoints += PointsPerClick * (_isMultiplierActive ? BoostMultiplier : 1f);

    public async Task ActivateMultiplier()
    {
        _isMultiplierActive = true;
        await Task.Delay(MultiplierActiveSeconds * 1000);
        _isMultiplierActive = false;
    }


}
