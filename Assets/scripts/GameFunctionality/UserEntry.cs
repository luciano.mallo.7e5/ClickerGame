using TMPro;
using UnityEngine;

public class UserEntry : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _userText, _scoreText;

    public void UpdateValues(string user, int score)
    {
        _userText.text = user;
        _scoreText.text = score.ToString();
    }
}
