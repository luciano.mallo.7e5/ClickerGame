# Podemos acceder a las diferentes operaciones de la siguiente manera:

- **To Strart server**: node server.js

- get all users:http://localhost:8000/players 

- add user: http://localhost:8000/add?userName=kojima&password=MetalGear

- get user data by userName: http://localhost:8000/player?userName=Alice&password=password123

- update userName: http://localhost:8000/update/player?userName=Dave&password=letmein&userName=David
/add/player?userName=Dave&password=letmein&userName=David

- update score of a user: http://localhost:8000/update/score?userName=Dave&score=51

- erase score of an user: http://localhost:8000/delete/score?userName=Dave
