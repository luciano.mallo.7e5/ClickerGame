const express = require("express");
const app = express();

const playersMocks = [
  {
    id: 1,
    userName: "Alice",
    password: "password123",
    score: 100,
  },
  {
    id: 2,
    userName: "Bob",
    password: "secret",
    score: 75,
  },
  {
    id: 3,
    userName: "Charlie",
    password: "qwerty",
    score: 90,
  },
  {
    id: 4,
    userName: "Dave",
    password: "letmein",
    score: 120,
  },
];

app.get("/players", (req, res) => {
  const sortedPlayers = playersMocks.sort((a, b) => b.score - a.score);
  res.json(sortedPlayers);
});

app.get("/player", (req, res) => {
  const userName = req.query.userName;
  const password = req.query.password;
  const player = playersMocks.find(
    (player) => player.userName === userName && player.password === password
  );
  if (player) {
    res.json(player);
  } else {
    res.status(400).json({ error: "Bad request" });
  }
});

app.get("/add", (req, res) => {
  const userName = req.query.userName;
  const password = req.query.password;
  if (!userName || !password) {
    res.status(400).json({ error: "Bad request" });
    return;
  }
  const newPlayer = {
    id: playersMocks.length + 1,
    userName,
    password,
    score: 0,
  };
  playersMocks.push(newPlayer);
  res.json(newPlayer);
});

app.get("/update/player", (req, res) => {
  const oldUserName = req.query.userName[0];
  const newUserName = req.query.userName[1];
  const userPassword = req.query.password;
  const playerIndex = playersMocks.findIndex(
    (player) => player.userName === oldUserName
  );
  if (playerIndex !== -1) {
    if (playersMocks[playerIndex].password === userPassword) {
      playersMocks[playerIndex].userName = newUserName;
      res.json(playersMocks[playerIndex]);
      return;
    }
  }
  res.status(400).json({ error: "Bad request" });
});

app.get("/update/score", (req, res) => {
  const userName = req.query.userName;
  const newScore = req.query.score;
  const playerIndex = playersMocks.findIndex(
    (player) => player.userName === userName
  );
  if (playerIndex !== -1) {
    playersMocks[playerIndex].score = newScore;
    res.json(playersMocks[playerIndex]);
  } else {
    res.status(400).json({ error: "Bad request" });
  }
});

app.get("/delete/score", (req, res) => {
  const userName = req.query.userName;
  const playerIndex = playersMocks.findIndex(
    (player) => player.userName === userName
  );
  if (playerIndex !== -1) {
    playersMocks[playerIndex].score = 0;
    res.json(playersMocks[playerIndex]);
  } else {
    res.status(400).json({ error: "Bad request" });
  }
});

const port = 8000;
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`)});